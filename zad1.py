# -*- coding: utf-8 -*-
import urllib2
import re

adres = raw_input('Podaj adres strony: ')
strona = urllib2.urlopen(adres)

zawartosc = strona.read()

zawartosc = zawartosc.decode('utf-8', 'ignore')

zawartosc = re.subn(r'<(script).*?</\1>(?s)', ' ', zawartosc)[0]
zawartosc = re.subn(r'<(style).*?</\1>(?s)', ' ', zawartosc)[0]
zawartosc = re.sub('&.*?;', ' ', zawartosc)
zawartosc = re.sub('\n', ' ', zawartosc)
zawartosc = re.sub('<.*?>', ' ', zawartosc)
zawartosc = re.sub('<!--.*?-->', ' ', zawartosc)
zawartosc = zawartosc.lower()
zawartosc = re.sub(u'[^a-z0-9ąęśćżźłóćń]', ' ', zawartosc)
zawartosc = re.sub('\s+', ' ', zawartosc)

with open('zawartosc_strony.txt', 'w') as fid:
    fid.write(zawartosc.encode('utf8'))
