# -*- coding: utf-8 -*-
import urllib2
import re
import time
import json
import requests

negative_list = []
positive_list = []

def getSentimentAnalysis(word):
    url = 'http://text-processing.com/api/sentiment/'
    data = dict(text=word)
    r = requests.post(url, data=data, allow_redirects=True)
   
    if r.json()['probability']['neg'] >= r.json()['probability']['neutral'] and r.json()['probability']['neg'] >= r.json()['probability']['pos']:
        tup = (r.json()['probability']['neg'], word)
        negative_list.append(tup)
        #print word
        #print ("Negatywna: " + str(r.json()['probability']['neg']))
    if r.json()['probability']['pos'] >= r.json()['probability']['neutral'] and r.json()['probability']['pos'] >= r.json()['probability']['neg']:
        tup = (r.json()['probability']['pos'], word)
        positive_list.append(tup)
        #print word 
        #print ("Pozytywna: " + str(r.json()['probability']['pos']))
       



k = 10
tresh = 4

address = raw_input('Podaj adres strony: ')
website = urllib2.urlopen(address)

website_content = website.read()

website_content = website_content.decode('utf-8', 'ignore')

website_content = re.subn(r'<(script).*?</\1>(?s)', ' ', website_content)[0]
website_content = re.subn(r'<(style).*?</\1>(?s)', ' ', website_content)[0]
website_content = re.sub('&.*?;', ' ', website_content)
website_content = re.sub('\n', ' ', website_content)
website_content = re.sub('<.*?>', ' ', website_content)
website_content = re.sub('<!--.*?-->', ' ', website_content)
website_content = website_content.lower()
website_content = re.sub(u'[^a-z0-9ąęśćżźłóćń]', ' ', website_content)
website_content = re.sub('\s+', ' ', website_content)
website_content = website_content.strip()

with open('website_content.txt', 'w') as fid:
    fid.write(website_content.encode('utf8'))

words_list = website_content.split(' ')
words_list.sort()
print "zaczynam przetwarzac"
for word in set(words_list):
    if len(word) >= tresh:
        getSentimentAnalysis(word)
print "skonczylem przetwarzac"        
positive_list.sort(reverse = True)
negative_list.sort(reverse = True)

print "Pozytywne:"
for word in positive_list[0:10]:
    print '-> ' + str(word[0]) + ' ' + word[1]

print "Negatywne:"
for word in negative_list[0:10]:
    print '-> ' + str(word[0]) + ' ' + word[1]
        
