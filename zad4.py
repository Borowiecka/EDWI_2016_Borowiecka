# -*- coding: utf-8 -*-
import urllib2
import re
import socket

N = 4

href_list = []
domain = ''
external_href_list = []
internal_href_list = []
external_file = ''
internal_file = ''


def get_domain(url_address):
   d=url_address
   if 'http' in url_address:
       d = url_address[url_address.find('//'):]
       d = d[2:]
   if url_address.startswith('//'):
       d = d[2:]
   if '/' in d:
       return d[:d.find('/')]  
   elif '?' in d:
       return d[:d.find('?')] 
   else:
       return d
   
      
def get_word_list(url_address):

   
   website = urllib2.urlopen(url_address)
   domain = get_domain(url_address)
   domain_ip = socket.gethostbyname(domain)
   
   website_content = website.read()
   website_content = website_content.decode('utf-8', 'ignore')
   
   website_content = re.findall(r'href=[\'"]?([^\'" >]+)', website_content)
   for i in website_content:
       if not i.startswith('#') and not i.startswith('android-app:') and not i.startswith('javascript:') and not i.startswith('ios-app:'):
           href_list.append(i)

   for href in href_list:
       if href.endswith('/'):
          href = href[:-1]
       if href not in internal_href_list and href not in external_href_list:
           print href
           if href.startswith('/') and not href.startswith('//'):
               internal_file.write('http://' + domain + href.encode('utf8') + '\n')
               internal_href_list.append('http://' + domain + href)
           else:
               href_domain = get_domain(href)  
               href_ip = socket.gethostbyname(href_domain)
               if href_ip == domain_ip:
                  #print '->' + href_domain + '    ' + href_ip + '    ' +  domain_ip
                  internal_file.write(href.encode('utf8') + '\n')
                  internal_href_list.append(href)
               else:
                  #print href_domain + '    ' + href_ip + '    ' +  domain_ip
                  external_file.write(href.encode('utf8') + '\n')
                  external_href_list.append(href)
               
     
           
    
external_file = open('external_href.txt', 'wb')
internal_file = open('interanl_href.txt', 'wb')

url = "http://weeia.p.lodz.pl/"
get_word_list(url)

for n in range(0,N):
    print "Now is checked: " + internal_href_list[n]
    get_word_list(internal_href_list[n])

external_file.close()
internal_file.close()
