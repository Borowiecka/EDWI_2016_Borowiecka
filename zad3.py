# -*- coding: utf-8 -*-
from __future__ import division
import urllib2
import re
import time
import numpy as np

k = 20
tresh = 20
all_word_list = []
all_sorted_lists = []
all_vectors = []

def get_word_list(url_address, name):
      
   website = urllib2.urlopen(url_address)
   
   website_content = website.read()

   website_content = website_content.decode('utf-8', 'ignore')

   website_content = re.subn(r'<(script).*?</\1>(?s)', ' ', website_content)[0]
   website_content = re.subn(r'<(style).*?</\1>(?s)', ' ', website_content)[0]
   website_content = re.sub('&.*?;', ' ', website_content)
   website_content = re.sub('\n', ' ', website_content)
   website_content = re.sub('<.*?>', ' ', website_content)
   website_content = re.sub('<!--.*?-->', ' ', website_content)
   website_content = website_content.lower()
   website_content = re.sub(u'[^a-z0-9ąęśćżźłóćń]', ' ', website_content)
   website_content = re.sub('\s+', ' ', website_content)

   with open(name+'.txt', 'w') as fid:
       fid.write(website_content.encode('utf8'))

   start_time = time.clock();
   words_list = website_content.split(' ')
   words_list.sort()
   sorted_list = {}

   for word in set(words_list):
      word_count = words_list.count(word)
      tup = (word_count, word)
      sorted_list[word]=word_count
      if len(word) > 1:
          if word not in all_word_list:
             all_word_list.append(word)
   #sorted_list.sort(reverse = True)
   tup = (name, sorted_list)
   all_sorted_lists.append(tup)
   
   #for word in sorted_list[0:10]:
   #    if word[0] >=tresh:
   #        final_list.append(word)
   #        print str(word[0]) + ' ' + word[1]
       
   end_time = time.clock();

   #print 'Calkowity czas: ' + str(end_time-start_time) + 's'
   
   
#url_address = raw_input('Podaj adres strony: ')
#get_word_list(url_address)

url_zoology1 = "https://pl.wikipedia.org/wiki/Hynobius_fucus"
url_zoology2 = "https://pl.wikipedia.org/wiki/Kr%C3%B3lik"
url_zoology3 = "https://pl.wikipedia.org/wiki/Robaki"
url_zoology4 = "https://pl.wikipedia.org/wiki/Ko%C5%84"
url_zoology5 = "https://pl.wikipedia.org/wiki/Bilbil_%C5%82ysy"

url_music1 = "https://pl.wikipedia.org/wiki/Muzyka_powa%C5%BCna"
url_music2 = "https://pl.wikipedia.org/wiki/Symfonia"
url_music3 = "https://pl.wikipedia.org/wiki/Antonio_Vivaldi"
url_music4 = "https://pl.wikipedia.org/wiki/Ludwig_van_Beethoven"
url_music5 = "https://pl.wikipedia.org/wiki/Opera"

url_informatics1 = "https://pl.wikipedia.org/wiki/Moc_obliczeniowa"
url_informatics2 = "https://pl.wikipedia.org/wiki/GPGPU"
url_informatics3 = "https://pl.wikipedia.org/wiki/Kliknij_tutaj"
url_informatics4 = "https://pl.wikipedia.org/wiki/Zawieszenie_komputera"
url_informatics5 = "https://pl.wikipedia.org/wiki/Informatyka_spo%C5%82eczna"


get_word_list(url_zoology1, "z1")
get_word_list(url_zoology2, "z2")
get_word_list(url_zoology3, "z3")
get_word_list(url_zoology4, "z4")
get_word_list(url_zoology5, "z5")
get_word_list(url_music1, "m1")
get_word_list(url_music2, "m2")
get_word_list(url_music3, "m3")
get_word_list(url_music4, "m4")
get_word_list(url_music5, "m5")
get_word_list(url_informatics1, "i1")
get_word_list(url_informatics2, "i2")
get_word_list(url_informatics3, "i3")
get_word_list(url_informatics4, "i4")
get_word_list(url_informatics5, "i5")

for sorted_list in all_sorted_lists:
   vector = []
   lenght = len(sorted_list[1])
   for word in all_word_list:
      if word in sorted_list[1]:
         word_count = sorted_list[1][word]
         count = word_count / lenght
         vector.append(count)
      else:
         vector.append(0)
   tup = (sorted_list[0], vector)
   all_vectors.append(tup)
   
for vector1 in all_vectors:
    for vector2 in all_vectors:
        vectorNP1 = np.array(vector1[1])
        vectorNP2 = np.array(vector2[1])
        x = np.dot(vectorNP1, vectorNP2)/(np.linalg.norm(vectorNP1)*np.linalg.norm(vectorNP2))
        print vector1[0] + " + " + vector2[0] + " - > " + str(x)
        























