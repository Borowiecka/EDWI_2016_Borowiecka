# -*- coding: utf-8 -*-
import urllib2
import re
import time
k = 10
tresh = 4
address = raw_input('Podaj adres strony: ')
website = urllib2.urlopen(address)

website_content = website.read()

website_content = website_content.decode('utf-8', 'ignore')

website_content = re.subn(r'<(script).*?</\1>(?s)', ' ', website_content)[0]
website_content = re.subn(r'<(style).*?</\1>(?s)', ' ', website_content)[0]
website_content = re.sub('&.*?;', ' ', website_content)
website_content = re.sub('\n', ' ', website_content)
website_content = re.sub('<.*?>', ' ', website_content)
website_content = re.sub('<!--.*?-->', ' ', website_content)
website_content = website_content.lower()
website_content = re.sub(u'[^a-z0-9ąęśćżźłóćń]', ' ', website_content)
website_content = re.sub('\s+', ' ', website_content)

with open('website_content.txt', 'w') as fid:
    fid.write(website_content.encode('utf8'))

start_time = time.clock();
words_list = website_content.split(' ')
words_list.sort()
sorted_list = []
final_list = []

for word in set(words_list):
   word_count = words_list.count(word)
   tup = (word_count, word)
   sorted_list.append(tup)
   
sorted_list.sort(reverse = True)

for word in sorted_list[0:10]:
    if word[0] >=tresh:
        final_list.append(word)
        print str(word[0]) + ' ' + word[1]
    
end_time = time.clock();

print 'Calkowity czas: ' + str(end_time-start_time) + 's'
